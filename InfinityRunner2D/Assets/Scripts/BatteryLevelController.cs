﻿using System;
using UnityEngine.UI;
using UnityEngine;

public class BatteryLevelController : MonoBehaviour
{
    private PlayerController player;
    private Text batteryText;
    private String _batteryText = "Bateria - {0}";

    // Start is called before the first frame update
    void Start()
    {
        batteryText = gameObject.GetComponent<Text>();
        player = FindObjectOfType<PlayerController>();
        batteryText.text = String.Format(_batteryText, "100");
        InvokeRepeating("DecreaseBatteryLevelCoroutine", 2f, 2f);
    }

    private void DecreaseBatteryLevelCoroutine()
    {
        player.LoseHealth(1);
        int counter = player.playerHealth;

        batteryText.text = String.Format(_batteryText, counter.ToString());
    }

}
