﻿using UnityEngine;

public class CameraMovement : MonoBehaviour
{

    private PlayerController player;
    private Vector2 lastPlayerPosition;
    private float distanceToMove;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        lastPlayerPosition = player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        distanceToMove = player.transform.position.y - lastPlayerPosition.y;

        transform.position = new Vector3(transform.position.x, transform.position.y + distanceToMove, transform.position.z);
    
        lastPlayerPosition = player.transform.position;
    }
}
