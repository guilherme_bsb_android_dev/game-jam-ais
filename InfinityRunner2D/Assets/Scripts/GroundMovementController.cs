﻿using UnityEngine;

public class GroundMovementController : MonoBehaviour
{
    public float speed;

    // Update is called once per frame
    void Update()
    {
        MoveScenarionBackwards();
    }

    private void MoveScenarionBackwards() 
    {
        transform.Translate(Vector2.left * speed * Time.deltaTime);
    }
}
