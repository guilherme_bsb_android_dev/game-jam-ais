﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public int playerHealth;
    public float jumpForce;
    public float jumpTime;
    private float jumpTimeCounter;
    public float landingForce;
    public bool isGrounded;
    private LayerMask groundLayerMask;
    private Rigidbody2D playerRigidbody;
    private BoxCollider2D playerCollider;

    // Start is called before the first frame update
    void Start()
    {
        playerRigidbody = gameObject.GetComponent<Rigidbody2D>();
        playerCollider = gameObject.GetComponent<BoxCollider2D>();
        groundLayerMask = LayerMask.GetMask("Ground");
        jumpTimeCounter = jumpTime;
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics2D.IsTouchingLayers(playerCollider, groundLayerMask);

        if (Input.GetKeyDown(KeyCode.Space)) 
        {
            Jump();
        }

        if (Input.GetKey(KeyCode.Space))
        {
            if(jumpTimeCounter > 0) {
                playerRigidbody.velocity = new Vector2(playerRigidbody.velocity.x, jumpForce);
                jumpTimeCounter -= Time.deltaTime;
            }
        }

        if(Input.GetKeyUp(KeyCode.Space)) 
        {
            jumpTimeCounter = 0;
        }
        
        if(Input.GetKeyDown(KeyCode.DownArrow) && !isGrounded) {
            playerRigidbody.velocity = Vector2.down * landingForce;
        } 

        if(isGrounded)
        {
            jumpTimeCounter = jumpTime; 
        }

    }

    private void Jump() 
    {
        if (isGrounded) {
            playerRigidbody.velocity = Vector2.up * jumpForce;
        }
    }

    public void LoseHealth(int healthToLose)
    {
        playerHealth -= healthToLose;
        if (playerHealth <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        Destroy(this);
        Time.timeScale = 0;
    }

}
